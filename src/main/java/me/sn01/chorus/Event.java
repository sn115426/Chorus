package me.sn01.chorus;
public class Event {
    boolean cancelled = false;


    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }


    public boolean isCancelled() {
        return cancelled;
    }
}