package me.sn01.chorus;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class EventManager {


        private final HashMap<Object, List<Method>> regMap = new HashMap<>();

        public void register(Object obj) {
            if (regMap.containsKey(obj)) return;
            else
                regMap.put(obj, Arrays.stream(obj.getClass().getDeclaredMethods()).filter(method -> method.isAnnotationPresent(ChorusEvent.class)).collect(Collectors.toList()));
        }

        public void unregister(Object obj) {
            regMap.remove(obj, Arrays.asList(obj.getClass().getDeclaredMethods()));
        }

        public void post(Event event) {
            regMap.forEach((o, methods) -> methods.forEach(method -> {
                if (method.getParameterTypes()[0] == event.getClass()) {
                    try {
                        if(event.isCancelled()) return;
                        method.invoke(o, event);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }));
        }

}
